# Confrontos no Youtube

## Instalação

```jsx
npm install
```

## Rodando localmente

```jsx
npm start
```

### URLs:

**Transmissão RJ:**

[http://localhost:3000/?confronto=9845&praca=rj&video=0](http://localhost:3000/?confronto=9845&praca=rj&video=0)

**Transmissão SP:**

[http://localhost:3000/?confronto=9845&praca=sp&video=0](http://localhost:3000/?confronto=9845&praca=rj&video=0)

### Parâmetros da URL

**confronto:** Parâmetro que recebe o ID do confronto cadastrado no Sistema de Serviços

**praca:** Parâmetro responsável por apresentar o layout específico de uma praça pois RJ e SP possuem grupos de patrocinadores diferentes. Os valores permitidos são "rj" e "sp"

**video:** Parâmetro responsável por apresentar o layout sem o componente de Scouts abrindo espaço para a inserção de um layer de vídeo pelo OBS. Os valores permitidos são "0" e "1".

## Gerando Build para Deploy

```jsx
npm run build
```