import React, { useMemo } from 'react';
import { useStore } from '../providers/Store';
import styled from 'styled-components'

const S = {
    Professionals: styled.ul`
    position: absolute;
    right: 20px;
    top: 30px;
    margin-right: 60px;
    `,
    
    Professional: styled.li`
    font-family: "ron";
    color: #FFF;
    position: relative;
    font-family: "ron";
    font-size: 42px;
    min-height: 60px;
    width: 100%;
    padding-left: 75px;
    list-style: none;
    @font-face {
        font-family: "ron";
        src: url("../fonts/ron.ttf");
    }
    &.caster {
        background: url("img/professionals/professional-caster.svg") left center no-repeat;
    }
    &.commentator {
        background: url("img/professionals/professional-commentator.svg") left center no-repeat;
    }
    &.reporter {
        background: url("img/professionals/professional-reporter.svg") left center no-repeat;
    }

    `
}


function Professionals() {
    const {state} = useStore();
    const {matchInfoSGR} = state;

    const professionalsList = matchInfoSGR.transmissoes[0].profissionais;

    const castersFilter = (items) => items.filter(item => item.tipo.slug === "narrador");
    const castersFilterMemo = useMemo(() => castersFilter(professionalsList), [professionalsList]);

    const commentatorsFilter = (items) => items.filter(item => item.tipo.slug === "comentarista");
    const commentatorsFilterMemo = useMemo(() => commentatorsFilter(professionalsList), [professionalsList]);

    const reportersFilter = (items) => items.filter(item => item.tipo.slug === "reporter");
    const reportersFilterMemo = useMemo(() => reportersFilter(professionalsList), [professionalsList]);

    
    return (
        <S.Professionals id="professionals" data-testid="professionals">
            {
                castersFilterMemo.map(filteredName => (
                    <S.Professional className="professional caster" key={filteredName.id.id}>
                        <span>{filteredName.id.nome}</span>
                    </S.Professional>
                ))
            }
            {
                commentatorsFilterMemo.map(filteredName => (
                    <S.Professional className="professional commentator" key={filteredName.id.id}>
                        <span>{filteredName.id.nome}</span>
                    </S.Professional>
                ))
            }
            {
                reportersFilterMemo.map(filteredName => (
                    <S.Professional className="professional reporter" key={filteredName.id.id}>
                        <span>{filteredName.id.nome}</span>
                    </S.Professional>
                ))
            }
        </S.Professionals>
    )
}

export default Professionals;