import React, {useEffect} from 'react';
import { useStore } from '../providers/Store';
import {getPlayers, getTeam, getGoals, getCards, getSubstitutions} from '../services/getSquadInfo';
import styled from 'styled-components';

const S = {
    SquadContainer: styled.div`
    font-family: "ron";
    width: 387px;
    border: 1px solid #969696;
    -ms-flex-item-align: start;
    align-self: start;
    `,

    TeamName: styled.div`
    background: #11110c;
    background: -webkit-gradient(left bottom, right top, color-stop(0%, #11110c), color-stop(100%, #555));
    background: linear-gradient(45deg, #11110c 0%, #555 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#11110c', endColorstr='#555555', GradientType=1 );
    font-family: "brasilia-short-ii";
    font-size: 40px;
    padding: 0 32px;
    line-height: 50px;
    border: 1px solid #969696;
    `,

    Row: styled.div`
    border-bottom: 1px solid #969696;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    &:nth-child(odd) {
        background-color: #f5f5f5;
    }
    &:nth-child(even) {
        background-color: #DEDEDE;
    }
    `,

    Number: styled.div`
    position: relative;
    display: -ms-flexbox;
    display: flex;
    width: 78px;
    font-size: 38px;
    background-color: #1C1C1B;
    -ms-flex-pack: end;
    justify-content: flex-end;
    padding: 0 16px;
    float: left;
    -ms-flex-preferred-size: 78px;
    flex-basis: 78px;
    `,

    Player: styled.div`
    font-size: 28px;
    color: #1C1C1B;
    text-transform: uppercase;
    padding: 0 12px;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: center;
    align-items: center;
    -ms-flex-preferred-size: 0;
    flex-basis: 0;
    -ms-flex-negative: 1;
    flex-shrink: 1;
    -ms-flex-positive: 1;
    flex-grow: 1;
    &.substitute {
        background-color: #8F8F8B !important;
    }
    `,

    Name: styled.span`
    -ms-flex-preferred-size: 100%;
    flex-basis: 100%;
    -ms-flex-negative: 1;
    flex-shrink: 1;
    -ms-flex-positive: 1;
    flex-grow: 1;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    `,

    Spacer: styled.div`
    -ms-flex-preferred-size: 100%;
    flex-basis: 100%;
    -ms-flex-negative: 0;
    flex-shrink: 0;
    -ms-flex-positive: 0;
    flex-grow: 0;
    border-bottom: 1px solid #969696;
    `,

    Card: styled.img`
    margin-right: 16px;
    `,

    TransferIcon: styled.img`
        margin-left: 8px;
    `,

    GoalIcon: styled.div`
        display: flex;
        align-content: center;
        font-family: arial;
        font-size: 20px;
        font-weight: lighter;
        white-space: nowrap;
        img {
            margin-right: 2px;
        }
    `
}


function SquadList(props) {
    const {state, dispatch} = useStore();
    const {matchInfoSGR, players, homeTeamPlayers, awayTeamPlayers, goals, cards, substitutions} = state;

    useEffect(() => {
        const fetchData = async () => {
            const players = await getPlayers(matchInfoSGR.id_footstats);
            dispatch({type:'setPlayers', payload: players});
            
            const goals = await getGoals(matchInfoSGR.id_footstats);
            dispatch({type:'setGoals', payload: goals});

            const cards = await getCards(matchInfoSGR.id_footstats);
            dispatch({type:'setCards', payload: cards});

            const substitutions = await getSubstitutions(matchInfoSGR.id_footstats);
            dispatch({type:'setSubstitutions', payload: substitutions});

            if (props.team === "homeTeam") {
                const homeTeamPlayers = await getTeam(props.teamId);
                dispatch({type:'setHomeTeamPlayers', payload: homeTeamPlayers});
            } else {
                const awayTeamPlayers = await getTeam(props.teamId);
                dispatch({type:'setAwayTeamPlayers', payload: awayTeamPlayers});
            };
        }
        fetchData()
        const interval = setInterval(() => {
            fetchData()
        }, 5000);
        return () => clearInterval(interval);
    
    }, [props.teamId, props.team, dispatch, matchInfoSGR.id_footstats])


    const mainPlayersFilter = (items) => items.filter(item => item.hasStarted === true);


    const getPlayerInfo = function(id,team){
        if(team){
            var playerInfo = team.filter(function(player) {
                return player.id === id
            })
            try {
                return playerInfo[0].nickname
            } catch(e) {
                console.log(e)
            }
            return "---";
        }
    }

    const getPlayerGoal = function(id,team) {
        if (goals) {
            var ownGoals = goals[team].filter(function(player) {
                return player.idPlayer === id && player.ownGoal;
            })
            var confirmedGoals = goals[team].filter(function(player) {
                return player.idPlayer === id && !player.varDecisionExclude && !player.ownGoal;
            })
            return {ownGoals, confirmedGoals};
        }
    }

    const getPlayerTransfer = function(id) {
        if (substitutions && homeTeamPlayers && awayTeamPlayers) {
            var result = substitutions.filter(function(transfer) {
                if(transfer.idPlayerOut === id){
                    return transfer
                }
                return false;
            })
            if(result.length > 0) {
                if(homeTeamPlayers || awayTeamPlayers){
                    var playerInfo = homeTeamPlayers.filter(function(player) {
                        return player.id === result[0].idPlayerIn;
                    })
                    if(!playerInfo[0]){
                        playerInfo = awayTeamPlayers.filter(function(player) {
                            return player.id === result[0].idPlayerIn;
                        })
                    }
                    return playerInfo[0];
                }
            }
            return false;
        }
    }

    const getPlayerCard = function(id, type) {
        if (cards) {
            var result = cards.filter(function(card) {
                if(card.idPlayer === id && card.cardType === type){
                    return card;
                }
                return false;
            })
            if(result.length > 0) {
                return result[0];
            }
            return false;
        }
    }
    
    return (
        <S.SquadContainer data-testid="squadList">
            <S.TeamName>
                <div>{props.team === "homeTeam" ? matchInfoSGR.time1.nome : matchInfoSGR.time2.nome}</div>
             </S.TeamName>
            {players?.[props.team] ? mainPlayersFilter(players?.[props.team]).map(filteredName => (
                <S.Row key={filteredName.id}>
                    <S.Number>
                        {
                            getPlayerCard(filteredName.idPlayer, 'YELLOW') ?
                                <S.Card src="img/match-events/card-yellow.svg" className="icon card yellow" alt="Cartão Amarelo" width="13" height="43" role="yellowcard" />
                            : "" 
                        }
                        {
                            getPlayerCard(filteredName.idPlayer, 'RED') ?
                                <S.Card src="img/match-events/card-red.svg" className="icon card red" alt="Cartão Vermelho" width="13" height="43" role="redcard" />
                            : ""
                        }
                        {
                            getPlayerCard(filteredName.idPlayer, 'YELLOW_RED') ?
                                <S.Card src="img/match-events/card-yellow-red.svg" className="icon card yellow-red" alt="Segundo Cartão Amarelo" width="13" height="43" role="yellowredcard" />
                            : ""
                        }
                        {filteredName.jerseyNumber}
                    </S.Number>
                    <S.Player>
                        <S.Name id={filteredName.idPlayer}>
                            {getPlayerInfo(filteredName.idPlayer, (props.team === "homeTeam" ? homeTeamPlayers : awayTeamPlayers))}
                        </S.Name>
                        { 
                            getPlayerGoal(filteredName.idPlayer, props.team) ?
                                getPlayerGoal(filteredName.idPlayer, props.team).confirmedGoals.length > 0 ? 
                                    <S.GoalIcon role="goal">
                                        <img src="img/match-events/goal.svg" className="icon goal" alt="Gol" width="22" height="22" />
                                        {
                                            getPlayerGoal(filteredName.idPlayer, props.team).confirmedGoals.length > 1 ? 
                                                "(" + getPlayerGoal(filteredName.idPlayer, props.team).confirmedGoals.length + ")"
                                            : ""
                                        }
                                    </S.GoalIcon>
                                : ""
                            : ""
                        }

                        { 
                            getPlayerGoal(filteredName.idPlayer, props.team) ?
                                getPlayerGoal(filteredName.idPlayer, props.team).ownGoals.length > 0 ? 
                                    <S.GoalIcon role="owngoal">
                                        <img src="img/match-events/goal-own.svg" className="icon own-goal" alt="Gol contra" width="22" height="22" />
                                        {
                                            getPlayerGoal(filteredName.idPlayer, props.team).ownGoals.length > 1 ? 
                                                "(" + getPlayerGoal(filteredName.idPlayer, props.team).ownGoals.length + ")"
                                            : ""
                                        }
                                    </S.GoalIcon>
                                : ""
                            : ""
                        }

                        { 
                            getPlayerTransfer(filteredName.idPlayer) ?
                                <S.TransferIcon src="img/match-events/transfer-out.svg" className="icon transfer out" alt="Substituição" width="18" height="31" />
                            : ""
                        }
                    </S.Player>
                    <S.Spacer></S.Spacer>
                        <S.Number className="number">
                        {
                            getPlayerCard(getPlayerTransfer(filteredName.idPlayer)?.id, 'YELLOW') ?
                                <S.Card src="img/match-events/card-yellow.svg" className="icon card yellow" alt="Cartão amarelo" width="13" height="43" role="yellowcard" />
                            : "" 
                        }
                        {
                            getPlayerCard(getPlayerTransfer(filteredName.idPlayer)?.id, 'RED') ?
                                <S.Card src="img/match-events/card-red.svg" className="icon card red" alt="Cartão vermelho" width="13" height="43"  role="redcard" />
                            : ""
                        }
                        {
                            getPlayerCard(getPlayerTransfer(filteredName.idPlayer)?.id, 'YELLOW_RED') ?
                                <S.Card src="img/match-events/card-yellow-red.svg" className="icon card yellow-red" alt="Segundo cartão amarelo" width="13" height="43" role="yellowredcard" />
                            : ""
                        }
                        {
                            getPlayerTransfer(filteredName.idPlayer) ? 
                                getPlayerTransfer(filteredName.idPlayer).jerseyNumber 
                            : ""
                        }
                        </S.Number>
                        <S.Player className="substitute" role="substitute">
                            <S.Name>
                                {
                                    getPlayerTransfer(filteredName.idPlayer) ? 
                                        getPlayerTransfer(filteredName.idPlayer).nickname 
                                    : ""
                                }
                            </S.Name>
                            { 
                                getPlayerGoal(getPlayerTransfer(filteredName.idPlayer)?.id, props.team) ?
                                    getPlayerGoal(getPlayerTransfer(filteredName.idPlayer)?.id, props.team).confirmedGoals.length > 0 ? 
                                        <S.GoalIcon role="goal">
                                            <img src="img/match-events/goal.svg" className="icon goal" alt="Gol" width="22" height="22" />
                                            {
                                                getPlayerGoal(getPlayerTransfer(filteredName.idPlayer)?.id, props.team).confirmedGoals.length > 1 ? 
                                                    "(" + getPlayerGoal(getPlayerTransfer(filteredName.idPlayer)?.id, props.team).confirmedGoals.length + ")"
                                                : ""
                                            }
                                        </S.GoalIcon>
                                    : ""
                                : ""
                            }

                            { 
                                getPlayerGoal(getPlayerTransfer(filteredName.idPlayer)?.id, props.team) ?
                                    getPlayerGoal(getPlayerTransfer(filteredName.idPlayer)?.id, props.team).ownGoals.length > 0 ? 
                                        <S.GoalIcon role="owngoal">
                                            <img src="img/match-events/goal-own.svg" className="icon own-goal" alt="Gol contra" width="22" height="22" />
                                            {
                                                getPlayerGoal(getPlayerTransfer(filteredName.idPlayer)?.id, props.team).ownGoals.length > 1 ? 
                                                    "(" + getPlayerGoal(getPlayerTransfer(filteredName.idPlayer)?.id, props.team).ownGoals.length + ")"
                                                : ""
                                            }
                                        </S.GoalIcon>
                                    : ""
                                : ""
                            }

                            {
                                getPlayerTransfer(filteredName.idPlayer) ? 
                                    <S.TransferIcon src="img/match-events/transfer-in.svg" className="icon transfer in" alt="Substituição" width="18" height="31" /> 
                                : ""
                            }
                        </S.Player>
                </S.Row>
                
            )) : ""}
        </S.SquadContainer>
    )
}

export default SquadList;