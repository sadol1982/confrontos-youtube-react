import { render, screen, waitFor } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import Championship from './Championship';
import { StoreProvider } from '../providers/Store.js';
import * as apiSGR from '../services/getMatchInfo';
import MockGetMatchInfoSGR from '../mocks/getMatchInfoSGR';

jest.mock('../services/getMatchInfo')

describe("Championship Component", ()=>{
    beforeEach(() => {
        jest.clearAllMocks();
      })

    it("Should render Championship Component", async () => {
        apiSGR.getMatchInfoSGR.mockResolvedValue(MockGetMatchInfoSGR);
        await act(async () => {
            render(<StoreProvider><Championship /></StoreProvider>);            
        });

        await screen.getByTestId("championship")
        expect(screen.getByTestId("championship")).toBeTruthy();
    }),

    it ("Should render championship name from api", async ()=>{
        apiSGR.getMatchInfoSGR.mockResolvedValue(MockGetMatchInfoSGR);
        await act(async () => {
            render(<StoreProvider><Championship /></StoreProvider>);            
        });

        await waitFor(()=>{
            expect(screen.getByText("Brasileirão")).toBeInTheDocument();
        })
    }),

    it ("Should render championship round name from api", async ()=>{
        apiSGR.getMatchInfoSGR.mockResolvedValue(MockGetMatchInfoSGR);
        await act(async () => {
            render(<StoreProvider><Championship /></StoreProvider>);            
        });

        await waitFor(()=>{
            expect(screen.getByText("34ª Rodada")).toBeInTheDocument();
        })
    })
})