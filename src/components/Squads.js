import React from 'react';
import { useStore } from '../providers/Store';
import SquadList from './SquadList';
import styled from 'styled-components';

const S = {
    HomeTeam: styled.div`
    position: absolute;
    left: 40px;
    top: 260px;
    `,

    AwayTeam: styled.div`
    position: absolute;
    right: 40px;
    top: 260px;
    `
}


function Squads() {
    const {state} = useStore();
    const {matchInfoFootstats} = state;

    
    return (
        <div data-testid="squads">
            <S.HomeTeam>
                <SquadList teamId={matchInfoFootstats.idTeamHome} team="homeTeam" data-testid="homeSquadList" />
            </S.HomeTeam>
            <S.AwayTeam>
                <SquadList teamId={matchInfoFootstats.idTeamAway} team="awayTeam" data-testid="awaySquadList" />
            </S.AwayTeam>
        </div>
    )
}

export default Squads;