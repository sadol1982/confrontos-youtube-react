import React from 'react';
import { useStore } from '../providers/Store';
import styled from 'styled-components'


const S = {
    Championship: styled.div`
    position: absolute;
    top: 40px;
    left: 60px;
    `,

    Title: styled.h1`
    font-family: "reddelicious";
    font-size: 60px;
    margin-bottom: 16px;
    color: #FFF;
    @font-face {
        font-family: "reddelicious";
        src: url("fonts/reddelicious.ttf");
    }
    `,
    
    Fase: styled.span`
    font-family: "brasilia-short-ii";
    color: #FFF;
    font-size: 40px;
    padding: 0 16px;
    background: #ffb12a;
    background: -webkit-gradient(left top, right top, color-stop(0%, #ffb12a), color-stop(100%, #fd6500));
    background: linear-gradient(to right, #ffb12a 0%, #fd6500 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffb12a', endColorstr='#fd6500', GradientType=1 );
    -webkit-box-decoration-break: clone;
    -ms-box-decoration-break: clone;
    -o-box-decoration-break: clone;
    box-decoration-break: clone;
    @font-face {
        font-family: "brasilia-short-ii";
        src: url("../fonts/brasilia-short-ii.ttf");
    }

    `
}


function Championship() {
    const {state} = useStore();
    const {matchInfoSGR} = state;

    return (
        <S.Championship data-testid="championship">
            <S.Title>{matchInfoSGR.campeonato.nome_exibicao ? matchInfoSGR.campeonato.nome_exibicao : matchInfoSGR.campeonato.nome}</S.Title>
            <div>
                {matchInfoSGR.fase ? <S.Fase>{matchInfoSGR.fase}</S.Fase> : ""}
                {matchInfoSGR.grupo ? <S.Fase>{matchInfoSGR.grupo}</S.Fase> : ""}
                {matchInfoSGR.rodada ? <S.Fase>{matchInfoSGR.rodada}ª Rodada</S.Fase> : ""}
            </div>
        </S.Championship>
    )
}

export default Championship;