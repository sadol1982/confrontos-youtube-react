import React, {useEffect} from 'react';
import { useStore } from '../providers/Store';
import {getScoutsFootstats} from '../services/getScouts';
import styled from 'styled-components';

const S = {
    Scouts: styled.div`
    position: absolute;
    align-items: center;
    top: 270px;
    left: 50%;
    transform: translateX(-50%);
    width: 1081px;
    height: 608px;
    `,

    Table: styled.table`
    width: 90%;
    margin: 0 auto;
    `,

    Th: styled.th`
    font-family: "brasilia-short-ii";
    font-size: 40px;
    padding: 15px;
    line-height: 50px;
    border-bottom: 2px solid #FFF;
    @font-face {
        font-family: "brasilia-short-ii";
        src: url("fonts/brasilia-short-ii.ttf");
    }
    `,

    Td: styled.td`
    padding: 10px;
    font-family: "ron";
    font-size: 32px;
    text-align: center;
    border-bottom: 1px solid rgba(255,255,255,0.5);
    `,

    Div: styled.div`
    display: inline;
    text-align: center;
    margin: 0 4px;
    `,

    Success: styled.div`
    display: inline-block;
    width: 64px;
    padding: 4px;
    text-align: center;
    border-radius: 5px;
    background-color: #059840;
    border: 1px solid #059840;
    margin: 0 4px;
    `,

    Wrong: styled.div`
    display: inline-block;
    width: 64px;
    padding: 4px;
    text-align: center;
    border-radius: 5px;
    background-color: #b8312f;
    border: 1px solid #b8312f;
    margin: 0 4px;
    `
}


function Scouts() {
    const {state, dispatch} = useStore();
    const {matchInfoSGR, scoutsFootstats} = state;

    const calcPercent = function(val1, val2) {
        var res = val1 / (val1 + val2) * 100;
        if (isNaN(res)) {
            return '0%';
          }
        return parseFloat(res).toFixed(0) + "%"
    }

    useEffect(() => {
        const fetchData = async () => {
            const scoutsFootstats = await getScoutsFootstats(matchInfoSGR.id_footstats);
            dispatch({type:'setScoutsFootstats', payload: scoutsFootstats});
        }
        const interval = setInterval(() => {
            fetchData()
        }, 5000);
        return () => clearInterval(interval);
      
    }, [dispatch, matchInfoSGR.id_footstats])


    return (
        <S.Scouts data-testid="scouts">
            <S.Table>
                <thead>
                    <tr>
                        <S.Th colSpan="3">scouts</S.Th>                        
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <S.Td>
                            <S.Div className="percent" role="scoutValue">
                                {calcPercent(scoutsFootstats?.homeTeam[0]?.ballPossession, scoutsFootstats?.awayTeam[0]?.ballPossession)}
                            </S.Div>
                        </S.Td>
                        <S.Td>Posse de Bola</S.Td>
                        <S.Td>
                            <S.Div className="percent" role="scoutValue">
                                {calcPercent(scoutsFootstats?.awayTeam[0]?.ballPossession, scoutsFootstats?.homeTeam[0]?.ballPossession)}
                            </S.Div>
                        </S.Td>
                    </tr>
                    <tr>
                        <S.Td>
                            <S.Success role="scoutValue">{scoutsFootstats?.homeTeam[0]?.passAccurate}</S.Success>
                            <S.Wrong role="scoutValue">{scoutsFootstats?.homeTeam[0]?.passNotAccurate}</S.Wrong>
                            <S.Div className="percent" role="scoutValue">
                                {calcPercent(scoutsFootstats?.homeTeam[0]?.passAccurate, scoutsFootstats?.homeTeam[0]?.passNotAccurate)}                                
                            </S.Div>
                        </S.Td>
                        <S.Td>Passes</S.Td>
                        <S.Td>
                            <S.Success role="scoutValue">{scoutsFootstats?.awayTeam[0]?.passAccurate}</S.Success>
                            <S.Wrong role="scoutValue">{scoutsFootstats?.awayTeam[0]?.passNotAccurate}</S.Wrong>
                            <S.Div className="percent" role="scoutValue">
                                {calcPercent(scoutsFootstats?.awayTeam[0]?.passAccurate, scoutsFootstats?.awayTeam[0]?.passNotAccurate)}                                
                            </S.Div>
                        </S.Td>
                    </tr>
                    <tr>
                        <S.Td>
                            <S.Success role="scoutValue">{scoutsFootstats?.homeTeam[0]?.shotOnTarget}</S.Success>
                            <S.Wrong role="scoutValue">{scoutsFootstats?.homeTeam[0]?.shotOffTarget}</S.Wrong>
                            <S.Div className="percent" role="scoutValue">
                                {calcPercent(scoutsFootstats?.homeTeam[0]?.shotOnTarget, scoutsFootstats?.homeTeam[0]?.shotOffTarget)}
                            </S.Div>
                        </S.Td>
                        <S.Td>Finalizações</S.Td>
                        <S.Td>
                            <S.Success role="scoutValue">{scoutsFootstats?.awayTeam[0]?.shotOnTarget}</S.Success>
                            <S.Wrong role="scoutValue">{scoutsFootstats?.awayTeam[0]?.shotOffTarget}</S.Wrong>
                            <S.Div className="percent" role="scoutValue">
                                {calcPercent(scoutsFootstats?.awayTeam[0]?.shotOnTarget, scoutsFootstats?.awayTeam[0]?.shotOffTarget)}
                            </S.Div>
                        </S.Td>
                    </tr>
                    <tr>
                        <S.Td>
                            <S.Success role="scoutValue">{scoutsFootstats?.homeTeam[0]?.tackleAccurate}</S.Success>
                            <S.Wrong role="scoutValue">{scoutsFootstats?.homeTeam[0]?.tackleNotAccurate}</S.Wrong>
                            <S.Div className="percent" role="scoutValue">
                                {calcPercent(scoutsFootstats?.homeTeam[0]?.tackleAccurate, scoutsFootstats?.homeTeam[0]?.tackleNotAccurate)}
                            </S.Div>
                        </S.Td>
                        <S.Td>Desarmes</S.Td>
                        <S.Td>
                            <S.Success role="scoutValue">{scoutsFootstats?.awayTeam[0]?.tackleAccurate}</S.Success>
                            <S.Wrong role="scoutValue">{scoutsFootstats?.awayTeam[0]?.tackleNotAccurate}</S.Wrong>
                            <S.Div className="percent" role="scoutValue">
                                {calcPercent(scoutsFootstats?.awayTeam[0]?.tackleAccurate, scoutsFootstats?.awayTeam[0]?.tackleNotAccurate)}
                            </S.Div>
                        </S.Td>
                    </tr>
                    <tr>
                        <S.Td>
                            <S.Success role="scoutValue">{scoutsFootstats?.homeTeam[0]?.sidePassAccurate}</S.Success>
                            <S.Wrong role="scoutValue">{scoutsFootstats?.homeTeam[0]?.sidePassNotAccurate}</S.Wrong>
                            <S.Div className="percent" role="scoutValue">
                                {calcPercent(scoutsFootstats?.homeTeam[0]?.sidePassAccurate, scoutsFootstats?.homeTeam[0]?.sidePassNotAccurate)}
                            </S.Div>
                        </S.Td>
                        <S.Td>Cruzamentos</S.Td>
                        <S.Td>
                            <S.Success role="scoutValue">{scoutsFootstats?.awayTeam[0]?.sidePassAccurate}</S.Success>
                            <S.Wrong role="scoutValue">{scoutsFootstats?.awayTeam[0]?.sidePassNotAccurate}</S.Wrong>
                            <S.Div className="percent" role="scoutValue">
                                {calcPercent(scoutsFootstats?.awayTeam[0]?.sidePassAccurate, scoutsFootstats?.awayTeam[0]?.sidePassNotAccurate)}
                            </S.Div>
                        </S.Td>
                    </tr>
                    <tr>
                        <S.Td>
                            <S.Success role="scoutValue">{scoutsFootstats?.homeTeam[0]?.longPassAccurate}</S.Success>
                            <S.Wrong role="scoutValue">{scoutsFootstats?.homeTeam[0]?.longPassNotAccurate}</S.Wrong>
                            <S.Div className="percent" role="scoutValue">
                                {calcPercent(scoutsFootstats?.homeTeam[0]?.longPassAccurate, scoutsFootstats?.homeTeam[0]?.longPassNotAccurate)}
                            </S.Div>
                        </S.Td>
                        <S.Td>Lançamentos</S.Td>
                        <S.Td>
                            <S.Success role="scoutValue">{scoutsFootstats?.awayTeam[0]?.longPassAccurate}</S.Success>
                            <S.Wrong role="scoutValue">{scoutsFootstats?.awayTeam[0]?.longPassNotAccurate}</S.Wrong>
                            <S.Div className="percent" role="scoutValue">
                                {calcPercent(scoutsFootstats?.awayTeam[0]?.longPassAccurate, scoutsFootstats?.awayTeam[0]?.longPassNotAccurate)}
                            </S.Div>
                        </S.Td>
                    </tr>
                    <tr>
                        <S.Td>
                            <S.Div className="text" role="scoutValue">{scoutsFootstats?.homeTeam[0]?.foul}</S.Div>
                        </S.Td>
                        <S.Td>Faltas Cometidas</S.Td>
                        <S.Td>
                            <S.Div className="text" role="scoutValue">{scoutsFootstats?.awayTeam[0]?.foul}</S.Div>
                        </S.Td>
                    </tr>
                    <tr>
                        <S.Td>
                            <S.Div className="text" role="scoutValue">{scoutsFootstats?.homeTeam[0]?.lossBallPossession}</S.Div>
                        </S.Td>
                        <S.Td>Perda de Posse de Bola</S.Td>
                        <S.Td>
                            <S.Div className="text" role="scoutValue">{scoutsFootstats?.awayTeam[0]?.lossBallPossession}</S.Div>
                        </S.Td>
                    </tr>
                </tbody>
            </S.Table>
        </S.Scouts>
    )
}

export default Scouts;