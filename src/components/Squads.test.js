import { render, screen } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import Squads from './Squads';
import { StoreProvider } from '../providers/Store.js';
import * as apiSGR from '../services/getMatchInfo';
import MockGetMatchInfoSGR from '../mocks/getMatchInfoSGR';
import MockGetMatchInfoFootstats from '../mocks/getMatchInfoFootstats';

jest.mock('../services/getMatchInfo');

describe("Squads Component", ()=>{
    beforeEach(() => {
        jest.clearAllMocks();
      })

    it("Should render Squads Component", async () => {
        apiSGR.getMatchInfoSGR.mockResolvedValue(MockGetMatchInfoSGR);
        apiSGR.getMatchInfoFootstats.mockResolvedValue(MockGetMatchInfoFootstats);
        await act(async () => {
            render(<StoreProvider><Squads /></StoreProvider>);
        });

        await screen.getByTestId("squads")
        expect(screen.getByTestId("squads")).toBeTruthy();
    })

})