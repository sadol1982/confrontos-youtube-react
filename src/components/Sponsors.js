import React, {useEffect, useState} from 'react';
import styled from 'styled-components';

const S = {
    Sponsors: styled.div`
    position: absolute;
    bottom: 20px;
    left: 465px;
    width: 992px;
    display: flex;
    flex-wrap: nowrap;
    align-items: center;
    justify-content: center;
    `,

    Sponsor: styled.div`
    background-color: #FFF;
    width: 180px;
    height: 135px;
    margin: 0px 10px;
    border-radius: 10px;
    overflow: hidden;
    `,
    
    Logo: styled.img`
    width: 110px;
    margin: 0px 40px;
    `,
}


function Sponsors() {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const region = urlParams.get('praca') === ("rj" || "sp") ? urlParams.get('praca') : "default";
        let data = {
            rj: [
                {id:1, name:"Mauá", path: "img/sponsors/maua.jpg"},
                {id:2, name:"Monroe", path: "img/sponsors/monroe.jpg"},
                {id:3, name:"Bet Nacional", path: "img/sponsors/bet-nacional.jpg"},
                {id:4, name:"Sicoob", path: "img/sponsors/sicoob.jpg"},
                {id:5, name:"Bet Nacional", path: "img/sponsors/bet-nacional.jpg"},
                {id:6, name:"Sicoob", path: "img/sponsors/sicoob.jpg"},
            ],
            sp: [
                {id:1, name:"Patriani", path: "img/sponsors/patriani.jpg"},
                {id:2, name:"Monroe", path: "img/sponsors/monroe.jpg"},
                {id:3, name:"Bet Nacional", path: "img/sponsors/bet-nacional.jpg"},
                {id:4, name:"Sicoob", path: "img/sponsors/sicoob.jpg"},
            ],
            default: []
        }


    const [sliceStart, setSliceStart] = useState(0);
    const [selectedSponsors, setSelectedSponsors] = useState([]) 
    const itemsPerPage = 4;

    useEffect(() => {
        if (data[region].length > itemsPerPage) {
            const sliceEnd = sliceStart + itemsPerPage;
            setSelectedSponsors(data[region].slice(sliceStart, sliceEnd));
        } else {
            setSelectedSponsors(data[region]);
        }
    // eslint-disable-next-line
    }, [sliceStart, region])

    useEffect(()=>{
        const interval = setInterval(() => {
            if(sliceStart + itemsPerPage >= data[region].length) {
                setSliceStart(0)
            } else {
                setSliceStart(sliceStart + itemsPerPage)
            }
        }, 5000);
        return () => clearInterval(interval);
    // eslint-disable-next-line
    },[sliceStart, region])


    return (
        <S.Sponsors data-testid="sponsors">
            <S.Logo src="img/logo.png" alt="Futebol Globo CBN" width="110" height="146" />
            { region === "rj" || "sp" ?
                selectedSponsors.map(item => (
                    <S.Sponsor key={item.id}>
                        <img src={item.path} alt={item.name} width="180" height="135" />
                    </S.Sponsor>
                ))
                : ""
            }
        </S.Sponsors>
    )
}

export default Sponsors;