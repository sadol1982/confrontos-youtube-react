import { render, screen } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import Sponsors from './Sponsors';


describe("Sponsors Component", ()=>{
    
    it("Should render with valid params", async () => {
        const location = {
            ...window.location,
            search: '?confronto=9845&praca=rj&video=0',
        };
        Object.defineProperty(window, 'location', {
            writable: true,
            value: location,
        })

        await act(async () => {
            render(<Sponsors />);            
        });

        await screen.getByTestId("sponsors")
        expect(screen.getByTestId("sponsors")).toBeTruthy();
    })

    it("Should render with invalid param", async () => {
        const location = {
            ...window.location,
            search: '?confronto=9845&praca=NULL&video=0',
        };
        Object.defineProperty(window, 'location', {
            writable: true,
            value: location,
        })

        await act(async () => {
            render(<Sponsors />);            
        });

        await screen.getByTestId("sponsors")
        expect(screen.getByTestId("sponsors")).toBeTruthy();
    })

    it("Should render with whithout param", async () => {
        const location = {
            ...window.location,
            search: '?confronto=9845&praca=NULL&video=0',
        };
        Object.defineProperty(window, 'location', {
            writable: true,
            value: location,
        })

        await act(async () => {
            render(<Sponsors />);            
        });

        await screen.getByTestId("sponsors")
        expect(screen.getByTestId("sponsors")).toBeTruthy();
    })
})