import React from 'react';
import { useStore } from '../providers/Store';
import styled from 'styled-components'

const S = {
    Match: styled.div`
    position: absolute;
    width: 700px;
    left: 50%;
    margin-left: -350px;
    top: 40px;
    display: flex;
    align-items: center;
    flex-wrap: wrap;
    justify-content: center;
  `,

    Score: styled.span`
    font-family: "brasilia-thin";
    font-size: 90px;
    color: #FFF;
    padding: 50px 16px 0;
    @font-face {
        font-family: "brasilia-thin";
        src: url("../fonts/brasilia-thin.ttf");
    }
  `,

    Stadium: styled.h1`
    font-family: "ron";
    font-size: 38px;
    text-align: center;
    width: 100%;
    color: #FFF;
    @font-face {
        font-family: "ron";
        src: url("fonts/ron.ttf");
    }
    & > small {
        display: block;
        font-size: 20px;
    }
  `,
  
    Fase: styled.span`
    font-family: "brasilia-short-ii";
    color: #FFF;
    font-size: 40px;
    padding: 0 16px;
    background: #ffb12a;
    background: -webkit-gradient(left top, right top, color-stop(0%, #ffb12a), color-stop(100%, #fd6500));
    background: linear-gradient(to right, #ffb12a 0%, #fd6500 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffb12a', endColorstr='#fd6500', GradientType=1 );
    -webkit-box-decoration-break: clone;
    -ms-box-decoration-break: clone;
    -o-box-decoration-break: clone;
    box-decoration-break: clone;
    @font-face {
        font-family: "brasilia-short-ii";
        src: url("../fonts/brasilia-short-ii.ttf");
    }
  `
}


function Score() {
    const { state } = useStore();
    const { matchInfoFootstats, matchInfoSGR } = state;


    return (
        <S.Match data-testid="score">
            <img src={matchInfoSGR?.time1?.escudo?.foto150x150} className="team home" alt={matchInfoSGR?.time1?.nome} width="150" height="150" data-testid="homeTeamImage" />
            <S.Score id="score-home" data-testid="homeScore">{matchInfoFootstats?.gameScore?.goalsHome}</S.Score>
            <img src="img/score-separator.svg" alt="X" width="26" height="29" />
            <S.Score id="score-visitor" data-testid="awayScore">{matchInfoFootstats?.gameScore?.goalsAway}</S.Score>
            <img src={matchInfoSGR?.time2?.escudo?.foto150x150} className="team home" alt={matchInfoSGR?.time2?.nome} width="150" height="150"  data-testid="awayTeamImage" />
            <S.Stadium>
                <small>Estádio</small>
                <span className="name">{matchInfoSGR?.estadio?.apelido}</span>
            </S.Stadium>
        </S.Match>
    )
}

export default Score;
