import { render, screen, waitFor } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import Professionals from './Professionals';
import { StoreProvider } from '../providers/Store.js';
import * as apiSGR from '../services/getMatchInfo';
import MockGetMatchInfoSGR from '../mocks/getMatchInfoSGR';

jest.mock('../services/getMatchInfo')

describe("Professionals Component", ()=>{
    beforeEach(() => {
        jest.clearAllMocks();
    })

    it("Should render Professionals Component", async () => {
        apiSGR.getMatchInfoSGR.mockResolvedValue(MockGetMatchInfoSGR);
        await act(async () => {
            render(<StoreProvider><Professionals /></StoreProvider>);            
        });

        await screen.getByTestId("professionals")
        expect(screen.getByTestId("professionals")).toBeTruthy();
    }),

    it ("Should render caster name from api", async ()=>{
        apiSGR.getMatchInfoSGR.mockResolvedValue(MockGetMatchInfoSGR);
        await act(async () => {
            render(<StoreProvider><Professionals /></StoreProvider>);            
        });

        await waitFor(()=>{
            expect(screen.getByText("Oscar Ulisses")).toBeInTheDocument();
        })
    })
})