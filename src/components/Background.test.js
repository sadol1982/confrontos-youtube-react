import { render, screen, waitFor } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import Background from './Background';
import { StoreProvider } from '../providers/Store.js';
import * as apiSGR from '../services/getMatchInfo';
import MockGetMatchInfoSGR from '../mocks/getMatchInfoSGR';

jest.mock('../services/getMatchInfo');

describe("Background Component", ()=>{
    beforeEach(() => {
        jest.clearAllMocks();
      })

    it("Should render Background Component", async () => {
        apiSGR.getMatchInfoSGR.mockResolvedValue(MockGetMatchInfoSGR);
        await act(async () => {
            render(<StoreProvider><Background /></StoreProvider>);            
        });

        await screen.getByTestId("background")
        expect(screen.getByTestId("background")).toBeTruthy();
    })

    it ("Should render championship related background image", async ()=>{
        apiSGR.getMatchInfoSGR.mockResolvedValue(MockGetMatchInfoSGR);
        await act(async () => {
            render(<StoreProvider><Background /></StoreProvider>);            
        });

        await waitFor(()=>{
            expect(screen.getByRole('image')).toHaveAttribute('src', 'img/backgrounds/brasileiro.jpg');
        });
    })
})