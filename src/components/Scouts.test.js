import { render, screen, waitFor } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import Scouts from './Scouts';
import { StoreProvider } from '../providers/Store.js';
import * as apiSGR from '../services/getMatchInfo';
import * as apiFootstatsScouts from '../services/getScouts';
import MockGetMatchInfoSGR from '../mocks/getMatchInfoSGR';
import MockGetScouts from '../mocks/getScouts';

jest.mock('../services/getMatchInfo');
jest.mock('../services/getScouts');

describe("Scouts Component", ()=>{
    beforeEach(() => {
        jest.clearAllMocks();
      })

    it("Should render Scouts Component", async () => {
        apiSGR.getMatchInfoSGR.mockResolvedValue(MockGetMatchInfoSGR);
        apiFootstatsScouts.getScoutsFootstats.mockResolvedValue(MockGetScouts);
        await act(async () => {
            render(<StoreProvider><Scouts /></StoreProvider>);            
        });

        await screen.getByTestId("scouts")
        expect(screen.getByTestId("scouts")).toBeTruthy();
    })

    it("Should be valid values", async () => {
        apiSGR.getMatchInfoSGR.mockResolvedValue(MockGetMatchInfoSGR);
        apiFootstatsScouts.getScoutsFootstats.mockResolvedValue(MockGetScouts);
        await act(async () => {
            render(<StoreProvider><Scouts /></StoreProvider>);            
        });
        await waitFor(()=>{
            expect(screen.getAllByRole("scoutValue")).not.toBeUndefined()
        })

    })

})