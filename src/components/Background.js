import React from "react";
import { useStore } from '../providers/Store';
import styled from 'styled-components'

const S = {
    Background: styled.picture`
    position: fixed;
    width: 1920px;
    height: 1080px;
    top: 0;
    left: 0;
    z-index:-1;
    `,
    
    Image: styled.img`
    height: inherit;
    width: inherit;
    `
}


function Background () {
    const {state} = useStore();
    const {matchInfoSGR} = state;

    const championshipId = matchInfoSGR?.campeonato?.id;

    function getBackground(id) {
        var obj = {
            1: "brasileiro",
            2: "carioca",
            3: "libertadores",
            6: "copabr",
            7: "paulista"
        };
        if(obj[id]){
            return obj[id];
        }
        return "default";
    }

    
    return (
        <S.Background data-testid="background">
            <S.Image src={`img/backgrounds/${getBackground(championshipId)}.jpg`} alt="Imagem de fundo" role="image"/>
        </S.Background>
    )
}

export default Background;