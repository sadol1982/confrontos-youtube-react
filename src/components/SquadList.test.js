import { render, screen, waitFor } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import SquadList from './SquadList';
import { StoreProvider } from '../providers/Store.js';
import * as apiSGR from '../services/getMatchInfo';
import * as apiFootstatsSquad from '../services/getSquadInfo';
import MockGetCards from '../mocks/getCards';
import MockGetGoals from '../mocks/getGoals';
import MockGetPlayers from '../mocks/getPlayers';
import MockGetSubstitutions from '../mocks/getSubstitutions';
import MockGetMatchInfoSGR from '../mocks/getMatchInfoSGR';

jest.mock('../services/getMatchInfo');
jest.mock('../services/getSquadInfo');

describe("SquadList Component", ()=>{
    beforeEach(() => {
       jest.clearAllMocks();
    })

    it("Should render SquadList Component", async () => {
        apiSGR.getMatchInfoSGR.mockResolvedValue(MockGetMatchInfoSGR);
        apiFootstatsSquad.getPlayers.mockResolvedValue(MockGetPlayers);
        apiFootstatsSquad.getGoals.mockResolvedValue(MockGetGoals);
        apiFootstatsSquad.getCards.mockResolvedValue(MockGetCards);
        apiFootstatsSquad.getSubstitutions.mockResolvedValue(MockGetSubstitutions);
        await act(async () => {
            render(<StoreProvider><SquadList /></StoreProvider>);            
        });

        await screen.getByTestId("squadList");
        expect(screen.getByTestId("squadList")).toBeTruthy();
    })

    it("Should be handling goals", async () => {
        apiSGR.getMatchInfoSGR.mockResolvedValue(MockGetMatchInfoSGR);
        apiFootstatsSquad.getPlayers.mockResolvedValue(MockGetPlayers);
        apiFootstatsSquad.getGoals.mockResolvedValue(MockGetGoals);

        await act(async () => {
            render(<StoreProvider><SquadList teamId="1010" team="homeTeam" /></StoreProvider>);            
        });

        await waitFor(()=>{
            expect(screen.getAllByRole("goal")).toBeInTheDocument;
            expect(screen.getAllByRole("owngoal")).toBeInTheDocument;
        });
    })

    it("Should be handling cards", async () => {
        apiSGR.getMatchInfoSGR.mockResolvedValue(MockGetMatchInfoSGR);
        apiFootstatsSquad.getPlayers.mockResolvedValue(MockGetPlayers);
        apiFootstatsSquad.getCards.mockResolvedValue(MockGetCards);

        await act(async () => {
            render(<StoreProvider><SquadList teamId="1012" team="awayTeam" /></StoreProvider>);
        });
        
        await waitFor(()=>{
            expect(screen.getAllByRole("yellowcard")).toBeInTheDocument;
            expect(screen.getAllByRole("redcard")).toBeInTheDocument;
            expect(screen.getAllByRole("yellowredcard")).toBeInTheDocument;
        })

    })

    it("Should be handling substitutions", async () => {
        apiSGR.getMatchInfoSGR.mockResolvedValue(MockGetMatchInfoSGR);
        apiFootstatsSquad.getPlayers.mockResolvedValue(MockGetPlayers);
        apiFootstatsSquad.getSubstitutions.mockResolvedValue(MockGetSubstitutions);

        await act(async () => {
            render(<StoreProvider><SquadList teamId="1012" team="awayTeam" /></StoreProvider>);
        });
        
        await waitFor(()=>{
            expect(screen.getAllByRole("substitute")).toBeInTheDocument;
        })

    })

})