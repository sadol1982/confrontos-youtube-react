import { render, screen, waitFor } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import Score from './Score';
import { StoreProvider } from '../providers/Store.js';
import * as apiSGR from '../services/getMatchInfo';
import MockGetMatchInfoSGR from '../mocks/getMatchInfoSGR';
import MockGetMatchInfoFootstats from '../mocks/getMatchInfoFootstats';

jest.mock('../services/getMatchInfo');

describe("Score Component", ()=>{
    beforeEach(() => {
        jest.clearAllMocks();
      })

    it("Should render Score Component", async () => {
        apiSGR.getMatchInfoSGR.mockResolvedValue(MockGetMatchInfoSGR);
        await act(async () => {
            render(<StoreProvider><Score /></StoreProvider>);            
        });

        await screen.getByTestId("score")
        expect(screen.getByTestId("score")).toBeTruthy();
    })

    it ("Should render teams images from api", async ()=>{
        apiSGR.getMatchInfoSGR.mockResolvedValue(MockGetMatchInfoSGR);
        await act(async () => {
            render(<StoreProvider><Score /></StoreProvider>);            
        });

        await waitFor(()=>{
            expect(screen.getByTestId('homeTeamImage')).toHaveAttribute('src', 'https://servico.globoradio.globo.com/uploads/fotos/2019/08/51ccb01d-b14f-486e-8540-0827829e0c6e.png.150x150_q75_box-0%2C0%2C490%2C490_crop_detail.png');
            expect(screen.getByTestId('homeTeamImage')).toHaveAttribute('alt', 'Athletico-PR');
            expect(screen.getByTestId('awayTeamImage')).toHaveAttribute('src', 'https://servico.globoradio.globo.com/uploads/fotos/2017/08/2c29dff4-9dc5-40a3-8a9a-72ce8e9e8833.png.150x150_q75_box-0%2C0%2C1080%2C1080_crop_detail.png');
            expect(screen.getByTestId('awayTeamImage')).toHaveAttribute('alt', 'Palmeiras');

        });
    }),

    it ("Should render stadium name from api", async ()=>{
        apiSGR.getMatchInfoSGR.mockResolvedValue(MockGetMatchInfoSGR);
        await act(async () => {
            render(<StoreProvider><Score /></StoreProvider>);
        });

        await waitFor(()=>{
            expect(screen.getByText("Arena da Baixada")).toBeInTheDocument();
        })
    })

    it ("Should render score from api", async ()=>{
        apiSGR.getMatchInfoSGR.mockResolvedValue(MockGetMatchInfoSGR);
        apiSGR.getMatchInfoFootstats.mockResolvedValue(MockGetMatchInfoFootstats);
        await act(async () => {
            render(<StoreProvider><Score /></StoreProvider>);
        })
        await waitFor(()=>{
            expect(screen.getByTestId("homeScore")).toHaveTextContent("1");
            expect(screen.getByTestId("awayScore")).toHaveTextContent("3");
        })
    })

})