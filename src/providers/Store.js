import React, {createContext, useContext, useReducer, useEffect} from 'react';
import {getMatchInfoSGR, getMatchInfoFootstats} from '../services/getMatchInfo';

const initialState = {
    matchInfoSGR: {
        time1: {
            escudo: {
                foto150x150: "",
            },
            nome: ""
        },
        time2: {
            escudo: {
                foto150x150: "",
            },
            nome: ""
        },
        estadio: {
            apelido: ""
        },
        id_footstats: "",
        campeonato: {
            nome: ""
        },
        transmissoes: [
                {
                profissionais: [
                    {
                        tipo: {
                            slug: ""
                        }
                    }
                ] 
                
            }
        ]
    },
    matchInfoFootstats: {
        gameScore: {
            goalsHome: "0",
            goalsAway: "0"
        }
    },
    scoutsFootstats: {
        awayTeam: [
            {
                ballPossession: "0",
                foul: "0",
                longPassAccurate: "0",
                longPassNotAccurate: "0",
                passAccurate: "0",
                passNotAccurate: "0",
                shotOnTarget: "0",
                shotOffTarget: "0",
                sidePassAccurate: "0",
                sidePassNotAccurate: "0",
                tackleAccurate: "0",
                tackleNotAccurate: "0"
            }
        ],
        homeTeam: [
            {
                ballPossession: "0",
                foul: "0",
                longPassAccurate: "0",
                longPassNotAccurate: "0",
                passAccurate: "0",
                passNotAccurate: "0",
                shotOnTarget: "0",
                shotOffTarget: "0",
                sidePassAccurate: "0",
                sidePassNotAccurate: "0",
                tackleAccurate: "0",
                tackleNotAccurate: "0"
            }
        ],
    },
    substitutions: ""
}


export const StoreContext = createContext();

function reducer(state, action) {
    switch (action.type) {
        case 'setMatchInfoSGR':
        return {
            ...state,
            matchInfoSGR: action.payload
        };
        case 'setMatchInfoFootstats':
        return {
            ...state,
            matchInfoFootstats: action.payload
        };
        case 'setScoutsFootstats':
        return {
            ...state,
            scoutsFootstats: action.payload
        };
        case 'setPlayers':
        return {
            ...state,
            players: action.payload
        };
        case 'setHomeTeamPlayers':
        return {
            ...state,
            homeTeamPlayers: action.payload
        };
        case 'setAwayTeamPlayers':
        return {
            ...state,
            awayTeamPlayers: action.payload
        };
        case 'setSubstitutions':
        return {
            ...state,
            substitutions: action.payload
        };
        case 'setGoals':
        return {
            ...state,
            goals: action.payload
        };
        case 'setCards':
        return {
            ...state,
            cards: action.payload
        };
        default:
        throw new Error();
    }
}

export const StoreProvider = ({children}) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const matchId = urlParams.get('confronto');

    useEffect(() => {
        const fetchData = async () => {
            const matchInfoSGR = await getMatchInfoSGR(matchId);
            dispatch({type:'setMatchInfoSGR', payload: matchInfoSGR});
            const matchInfoFootstats = await getMatchInfoFootstats(matchInfoSGR.id_footstats);
            dispatch({type:'setMatchInfoFootstats', payload: matchInfoFootstats});
        }
        
        fetchData()

        const interval = setInterval(() => {
            fetchData()
        }, 5000);
        return () => clearInterval(interval);
        
    // eslint-disable-next-line
    }, [])

    
    return (
        <StoreContext.Provider value={{state, dispatch, matchId}}>
            {children}
        </StoreContext.Provider>
    )
}

export function useStore(){
    const context = useContext(StoreContext)
    return context;
}
