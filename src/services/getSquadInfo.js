const config = {
    headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json"
    },   
}

export async function getPlayers(matchId) {
    const response = await fetch(`https://globoradio.globo.com/apifootstats/matches/${matchId}/formation/`, config)
    const players = await response.json();
    return players.data
}

export async function getTeam(idTeam) {
    const response = await fetch(`https://globoradio.globo.com/apifootstats/teams/${idTeam}/players/`, config)
    const team = await response.json();
    return team.data
}

export async function getSubstitutions(matchId) {
    const response = await fetch(`https://globoradio.globo.com/apifootstats/matches/${matchId}/substitutions/`, config)
    const substitutions = await response.json();
    return substitutions.data
}

export async function getGoals(matchId) {
    const response = await fetch(`https://globoradio.globo.com/apifootstats/matches/${matchId}/goals/`, config)
    const goals = await response.json();
    return goals.data
}

export async function getCards(matchId) {
    const response = await fetch(`https://globoradio.globo.com/apifootstats/matches/${matchId}/cards/`, config)
    const cards = await response.json();
    return cards.data
}