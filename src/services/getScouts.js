const config = {
    headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json"
    },   
}

export async function getScoutsFootstats(id) {
    const response = await fetch(`https://globoradio.globo.com/apifootstats/matches/${id}/teams/scout/`, config)
    const scoutsFootstats = await response.json();
    return scoutsFootstats.data
}