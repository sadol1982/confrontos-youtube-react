const config = {
    headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json"
    },   
}

export async function getMatchInfoSGR(id) {
    const response = await fetch(`https://servico.globoradio.globo.com/api/v1/confronto/confronto/${id}`)
    const matchInfoSGR = await response.json();
    return matchInfoSGR
}

export async function getMatchInfoFootstats(id) {
    const response = await fetch(`https://globoradio.globo.com/apifootstats/matches/${id}/`, config)
    const matchInfoFootstats = await response.json();
    return matchInfoFootstats.data
}
