import Championship from './components/Championship';
import Score from './components/Score';
import Professionals from './components/Professionals';
import Scouts from './components/Scouts';
import Squads from './components/Squads';
import Background from './components/Background';
import Sponsors from './components/Sponsors';

function App() {
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const videoMode = urlParams.get('video');
  return (
    <div className="App">
      <Championship />
      <Score />
      <Professionals />
      <Squads />
      {videoMode === "1" ? "" : <Scouts />}
      <Sponsors></Sponsors>
      <Background />
    </div>
  );
}

export default App;
