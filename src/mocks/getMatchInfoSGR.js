const getMatchInfoSGR = {
    "id": 9845,
    "confronto": "Athletico-PR x Palmeiras",
    "confronto_placar": "Athletico-PR 1 x 3 Palmeiras",
    "url": "https://futebolglobocbn.globo.com/jogo/26/10/2022/athletico-pr-x-palmeiras-9845.htm",
    "slug": "athletico-pr-x-palmeiras",
    "data_inicio": "2022-10-26T00:45:00Z",
    "data_fim": "2022-10-26T02:58:00Z",
    "placar": "1 x 3",
    "time1": {
        "id": 235,
        "escudo": {
            "id": 1908,
            "foto22x22": "https://servico.globoradio.globo.com/uploads/fotos/2019/08/51ccb01d-b14f-486e-8540-0827829e0c6e.png.22x22_q75_box-0%2C0%2C490%2C490_crop_detail.png",
            "foto36x36": "https://servico.globoradio.globo.com/uploads/fotos/2019/08/51ccb01d-b14f-486e-8540-0827829e0c6e.png.36x36_q75_box-0%2C0%2C490%2C490_crop_detail.png",
            "foto150x150": "https://servico.globoradio.globo.com/uploads/fotos/2019/08/51ccb01d-b14f-486e-8540-0827829e0c6e.png.150x150_q75_box-0%2C0%2C490%2C490_crop_detail.png",
            "titulo": "Novo escudo Athletico Paranaense",
            "foto": "https://servico.globoradio.globo.com/uploads/fotos/2019/08/51ccb01d-b14f-486e-8540-0827829e0c6e.png"
        },
        "nome": "Athletico-PR",
        "sigla": "CAP",
        "selecao": false
    },
    "time2": {
        "id": 95,
        "escudo": {
            "id": 590,
            "foto22x22": "https://servico.globoradio.globo.com/uploads/fotos/2017/08/2c29dff4-9dc5-40a3-8a9a-72ce8e9e8833.png.22x22_q75_box-0%2C0%2C1080%2C1080_crop_detail.png",
            "foto36x36": "https://servico.globoradio.globo.com/uploads/fotos/2017/08/2c29dff4-9dc5-40a3-8a9a-72ce8e9e8833.png.36x36_q75_box-0%2C0%2C1080%2C1080_crop_detail.png",
            "foto150x150": "https://servico.globoradio.globo.com/uploads/fotos/2017/08/2c29dff4-9dc5-40a3-8a9a-72ce8e9e8833.png.150x150_q75_box-0%2C0%2C1080%2C1080_crop_detail.png",
            "titulo": "escudo-palmeiras",
            "foto": "https://servico.globoradio.globo.com/uploads/fotos/2017/08/2c29dff4-9dc5-40a3-8a9a-72ce8e9e8833.png"
        },
        "nome": "Palmeiras",
        "sigla": "PAL",
        "selecao": false
    },
    "placar_time1": 1,
    "placar_penalti_time1": null,
    "placar_time2": 3,
    "placar_penalti_time2": null,
    "campeonato": {
        "id": 1,
        "nome": "Campeonato Brasileiro - Série A",
        "nome_exibicao": "Brasileirão",
        "slug": "brasileirao"
    },
    "estadio": {
        "id": 105,
        "nome": "Arena da Baixada",
        "apelido": "Arena da Baixada",
        "local": "Curitiba-PR"
    },
    "temporada": 2022,
    "grupo": "",
    "rodada": 34,
    "fase": null,
    "flag_no_ar": false,
    "flag_pre_jogo": false,
    "flag_pos_jogo": false,
    "transmissoes": [
        {
            "profissionais": [
                {
                    "id": {
                        "id": 22,
                        "nome": "Oscar Ulisses",
                        "descricao": "",
                        "email": "oscarulisses@radioglobo.com.br",
                        "foto": {
                            "id": 784,
                            "thumb": "https://servico.globoradio.globo.com/uploads/fotos/2017/11/cf50c966-5c66-4a93-a269-98ff72aa5d30.png.320x180_q75_box-0%2C0%2C1920%2C1080_crop_detail.png",
                            "fotoMediaRes": "https://servico.globoradio.globo.com/uploads/fotos/2017/11/cf50c966-5c66-4a93-a269-98ff72aa5d30.png.400x400_q75_box-420%2C0%2C1500%2C1080_crop_detail.png",
                            "fotoAltaRes": "https://servico.globoradio.globo.com/uploads/fotos/2017/11/cf50c966-5c66-4a93-a269-98ff72aa5d30.png.1080x1080_q75_box-420%2C0%2C1500%2C1080_crop_detail.png",
                            "foto4x3": "https://servico.globoradio.globo.com/uploads/fotos/2017/11/cf50c966-5c66-4a93-a269-98ff72aa5d30.png.800x600_q75_box-0%2C0%2C1920%2C1080_crop_detail.png",
                            "foto16x9": "https://servico.globoradio.globo.com/uploads/fotos/2017/11/cf50c966-5c66-4a93-a269-98ff72aa5d30.png.960x540_q75_box-0%2C0%2C1920%2C1080_crop_detail.png",
                            "credito": null,
                            "titulo": "oscar-ulisses",
                            "foto": "https://servico.globoradio.globo.com/uploads/fotos/2017/11/cf50c966-5c66-4a93-a269-98ff72aa5d30.png",
                            "observacao": "",
                            "site": 36
                        },
                        "slug": "oscar-ulisses"
                    },
                    "tipo": {
                        "id": 7,
                        "nome": "Narrador",
                        "slug": "narrador"
                    }
                },
                {
                    "id": {
                        "id": 79,
                        "nome": "Gabriel Dudziak",
                        "descricao": "",
                        "email": "gabriel.dudziak@cbn.com.br",
                        "foto": {
                            "id": 770,
                            "thumb": "https://servico.globoradio.globo.com/uploads/fotos/2017/11/7e66e9d7-8028-4b76-98a4-df28fd5daa4e.png.320x180_q75_box-0%2C0%2C1920%2C1080_crop_detail.png",
                            "fotoMediaRes": "https://servico.globoradio.globo.com/uploads/fotos/2017/11/7e66e9d7-8028-4b76-98a4-df28fd5daa4e.png.400x400_q75_box-420%2C0%2C1500%2C1080_crop_detail.png",
                            "fotoAltaRes": "https://servico.globoradio.globo.com/uploads/fotos/2017/11/7e66e9d7-8028-4b76-98a4-df28fd5daa4e.png.1080x1080_q75_box-420%2C0%2C1500%2C1080_crop_detail.png",
                            "foto4x3": "https://servico.globoradio.globo.com/uploads/fotos/2017/11/7e66e9d7-8028-4b76-98a4-df28fd5daa4e.png.800x600_q75_box-0%2C0%2C1920%2C1080_crop_detail.png",
                            "foto16x9": "https://servico.globoradio.globo.com/uploads/fotos/2017/11/7e66e9d7-8028-4b76-98a4-df28fd5daa4e.png.960x540_q75_box-0%2C0%2C1920%2C1080_crop_detail.png",
                            "credito": null,
                            "titulo": "gabriel-dudziak",
                            "foto": "https://servico.globoradio.globo.com/uploads/fotos/2017/11/7e66e9d7-8028-4b76-98a4-df28fd5daa4e.png",
                            "observacao": "",
                            "site": 36
                        },
                        "slug": "gabriel-dudziak"
                    },
                    "tipo": {
                        "id": 2,
                        "nome": "Comentarista",
                        "slug": "comentarista"
                    }
                },
                {
                    "id": {
                        "id": 390,
                        "nome": "Raphael Prates",
                        "descricao": "",
                        "email": "a@a",
                        "foto": {
                            "id": 802,
                            "thumb": "https://servico.globoradio.globo.com/uploads/fotos/2017/11/94b8f6d4-ae2e-4f95-a7ec-6533aea8ec50.png.320x180_q75_box-0%2C0%2C1920%2C1080_crop_detail.png",
                            "fotoMediaRes": "https://servico.globoradio.globo.com/uploads/fotos/2017/11/94b8f6d4-ae2e-4f95-a7ec-6533aea8ec50.png.400x400_q75_box-420%2C0%2C1500%2C1080_crop_detail.png",
                            "fotoAltaRes": "https://servico.globoradio.globo.com/uploads/fotos/2017/11/94b8f6d4-ae2e-4f95-a7ec-6533aea8ec50.png.1080x1080_q75_box-420%2C0%2C1500%2C1080_crop_detail.png",
                            "foto4x3": "https://servico.globoradio.globo.com/uploads/fotos/2017/11/94b8f6d4-ae2e-4f95-a7ec-6533aea8ec50.png.800x600_q75_box-0%2C0%2C1920%2C1080_crop_detail.png",
                            "foto16x9": "https://servico.globoradio.globo.com/uploads/fotos/2017/11/94b8f6d4-ae2e-4f95-a7ec-6533aea8ec50.png.960x540_q75_box-0%2C0%2C1920%2C1080_crop_detail.png",
                            "credito": null,
                            "titulo": "rafael-prates",
                            "foto": "https://servico.globoradio.globo.com/uploads/fotos/2017/11/94b8f6d4-ae2e-4f95-a7ec-6533aea8ec50.png",
                            "observacao": "",
                            "site": 36
                        },
                        "slug": "raphael-prates"
                    },
                    "tipo": {
                        "id": 2,
                        "nome": "Comentarista",
                        "slug": "comentarista"
                    }
                }
            ],
            "stream": {
                "nome": "FCBN SP",
                "nome_exibicao": "São Paulo",
                "descricao": "",
                "foto": null,
                "icone": null,
                "urls": [
                    {
                        "protocolo": {
                            "nome": "icecast"
                        },
                        "url": "https://playerservices.streamtheworld.com/api/livestream-redirect/CBN_SPAAC.aac",
                        "bitrate": {
                            "nome": "48kbps",
                            "valor": 48
                        }
                    }
                ],
                "slug": "fcbn-sp"
            }
        }
    ],
    "whitelabel_footstats": null,
    "id_footstats": 199677
}

export default getMatchInfoSGR;