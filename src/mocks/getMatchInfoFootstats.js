const getMatchInfoFootstats = {
        "attendance": null,
        "championship": "Brasileiro Série A 2022",
        "coachAway": "Abel Ferreira",
        "coachHome": "Luiz Felipe Scolari",
        "cupName": "Brasileiro Série A 2022",
        "date": {
            "chronology": {
                "calendarType": "iso8601",
                "id": "ISO"
            },
            "dayOfMonth": 25,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 298,
            "hour": 21,
            "minute": 45,
            "month": "OCTOBER",
            "monthValue": 10,
            "nano": 0,
            "second": 0,
            "year": 2022
        },
        "dateUndefined": false,
        "finished": true,
        "gameScore": {
            "awayLoss": false,
            "awayWin": true,
            "draw": false,
            "goalsAway": 3,
            "goalsAwayByWo": null,
            "goalsHome": 1,
            "goalsHomeByWo": null,
            "homeLoss": true,
            "homeWin": false,
            "penaltiesAway": 0,
            "penaltiesDecisionAwayGoal": 0,
            "penaltiesDecisionAwayLoss": 0,
            "penaltiesDecisionHomeGoal": 0,
            "penaltiesDecisionHomeLoss": 0,
            "penaltiesHome": 0
        },
        "gameTime": "Final",
        "hasCoachAwayRedCard": false,
        "hasCoachAwayYellowCard": false,
        "hasCoachHomeRedCard": false,
        "hasCoachHomeYellowCard": true,
        "hasNarration": true,
        "hasScout": true,
        "hourUndefined": false,
        "id": 199677,
        "idChampionship": 803,
        "idCoachAway": 2519,
        "idCoachHome": 3,
        "idGameTime": 11,
        "idReferee": 781,
        "idStadium": 8,
        "idTeamAway": 1012,
        "idTeamHome": 1010,
        "inserted": "2022-02-03T22:44:21.553",
        "isoDate": "2022-10-25T21:45:00",
        "matchGroup": null,
        "numberStaffAwayRedCard": 0,
        "numberStaffAwayYellowCard": 0,
        "numberStaffHomeRedCard": 0,
        "numberStaffHomeYellowCard": 0,
        "phase": "Primeira Fase",
        "playOffKey": null,
        "realtime": false,
        "referee": "Braulio da Silva Machado",
        "round": 34,
        "sde": {
            "arbitro_auxiliar_1_id": null,
            "arbitro_auxiliar_2_id": null,
            "arbitro_principal_id": null,
            "equipe_mandante_id": 293,
            "equipe_visitante_id": 275,
            "jogo_id": 287657,
            "sede_id": 403,
            "tecnico_mandante_id": 43618,
            "tecnico_visitante_id": 110506
        },
        "sequenceMatch": 0,
        "stadium": "Arena da Baixada",
        "teamAway": "Palmeiras",
        "teamHome": "Athletico-PR",
        "ticketCollected": null,
        "updated": "2022-10-26T03:57:29.02"
}

export default getMatchInfoFootstats;