const getSubstitutions = [
    {
        "id": 36637589,
        "idGameTime": 3,
        "idMatch": 199677,
        "idPlayerIn": 50759,
        "idPlayerOut": 76119,
        "idTeam": 1010,
        "timePlayInSeconds": 3047
    },
    {
        "id": 36637565,
        "idGameTime": 4,
        "idMatch": 199677,
        "idPlayerIn": 52411,
        "idPlayerOut": 20232,
        "idTeam": 1012,
        "timePlayInSeconds": 2322
    },
    {
        "id": 36637566,
        "idGameTime": 4,
        "idMatch": 199677,
        "idPlayerIn": 63658,
        "idPlayerOut": 16658,
        "idTeam": 1012,
        "timePlayInSeconds": 2310
    },
    {
        "id": 36637592,
        "idGameTime": 4,
        "idMatch": 199677,
        "idPlayerIn": 65273,
        "idPlayerOut": 84532,
        "idTeam": 1010,
        "timePlayInSeconds": 1659
    },
    {
        "id": 36637596,
        "idGameTime": 4,
        "idMatch": 199677,
        "idPlayerIn": 67363,
        "idPlayerOut": 89113,
        "idTeam": 1010,
        "timePlayInSeconds": 126
    },
    {
        "id": 36637591,
        "idGameTime": 4,
        "idMatch": 199677,
        "idPlayerIn": 67620,
        "idPlayerOut": 66591,
        "idTeam": 1010,
        "timePlayInSeconds": 1272
    },
    {
        "id": 36637567,
        "idGameTime": 4,
        "idMatch": 199677,
        "idPlayerIn": 69745,
        "idPlayerOut": 50804,
        "idTeam": 1012,
        "timePlayInSeconds": 1875
    },
    {
        "id": 36637594,
        "idGameTime": 4,
        "idMatch": 199677,
        "idPlayerIn": 71643,
        "idPlayerOut": 41313,
        "idTeam": 1010,
        "timePlayInSeconds": 1257
    },
    {
        "id": 36637571,
        "idGameTime": 3,
        "idMatch": 199677,
        "idPlayerIn": 80548,
        "idPlayerOut": 80693,
        "idTeam": 1012,
        "timePlayInSeconds": 3047
    },
    {
        "id": 36637573,
        "idGameTime": 4,
        "idMatch": 199677,
        "idPlayerIn": 88493,
        "idPlayerOut": 34842,
        "idTeam": 1012,
        "timePlayInSeconds": 9
    }
]

export default getSubstitutions;