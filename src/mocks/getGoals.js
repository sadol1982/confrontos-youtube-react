const getGoals = {
    "awayTeam": [
        {
            "id": 68990205,
            "idGameTime": 4,
            "idMatch": 199677,
            "idPlayer": 88493,
            "idTeam": 1012,
            "ownGoal": false,
            "timePlayInMinutes": 13,
            "timePlayInSeconds": 803,
            "varDecisionExclude": false,
            "varDecisionInclude": false
        },
        {
            "id": 68990915,
            "idGameTime": 4,
            "idMatch": 199677,
            "idPlayer": 88493,
            "idTeam": 1012,
            "ownGoal": false,
            "timePlayInMinutes": 24,
            "timePlayInSeconds": 1498,
            "varDecisionExclude": false,
            "varDecisionInclude": false
        },
        {
            "id": 68991301,
            "idGameTime": 4,
            "idMatch": 199677,
            "idPlayer": 43254,
            "idTeam": 1012,
            "ownGoal": false,
            "timePlayInMinutes": 30,
            "timePlayInSeconds": 1813,
            "varDecisionExclude": false,
            "varDecisionInclude": false
        }
    ],
    "homeTeam": [
        {
            "id": 68987821,
            "idGameTime": 2,
            "idMatch": 199677,
            "idPlayer": 70293,
            "idTeam": 1010,
            "ownGoal": false,
            "timePlayInMinutes": 29,
            "timePlayInSeconds": 1795,
            "varDecisionExclude": false,
            "varDecisionInclude": false
        },
        {
            "id": 68987822,
            "idGameTime": 2,
            "idMatch": 199677,
            "idPlayer": 85262,
            "idTeam": 1010,
            "ownGoal": true,
            "timePlayInMinutes": 30,
            "timePlayInSeconds": 1795,
            "varDecisionExclude": false,
            "varDecisionInclude": false
        },
    ],
    "idMatch": 199677
}

export default getGoals;