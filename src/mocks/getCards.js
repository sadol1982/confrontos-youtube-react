const getCards = [
    {
        "cardType": "YELLOW",
        "id": 68986126,
        "idGameTime": 2,
        "idMatch": 199677,
        "idPlayer": 80611,
        "idTeam": 1010,
        "timePlayInMinutes": 7,
        "timePlayInSeconds": 447,
        "varDecisionExclude": false,
        "varDecisionInclude": false
    },
    {
        "cardType": "YELLOW_RED",
        "id": 68986298,
        "idGameTime": 2,
        "idMatch": 199677,
        "idPlayer": 43254,
        "idTeam": 1012,
        "timePlayInMinutes": 9,
        "timePlayInSeconds": 585,
        "varDecisionExclude": false,
        "varDecisionInclude": false
    },
    {
        "cardType": "YELLOW",
        "id": 68989353,
        "idGameTime": 4,
        "idMatch": 199677,
        "idPlayer": 84532,
        "idTeam": 1010,
        "timePlayInMinutes": 1,
        "timePlayInSeconds": 105,
        "varDecisionExclude": false,
        "varDecisionInclude": false
    },
    {
        "cardType": "YELLOW",
        "id": 68989816,
        "idGameTime": 4,
        "idMatch": 199677,
        "idPlayer": 61887,
        "idTeam": 1012,
        "timePlayInMinutes": 7,
        "timePlayInSeconds": 420,
        "varDecisionExclude": false,
        "varDecisionInclude": false
    },
    {
        "cardType": "YELLOW",
        "id": 68990115,
        "idGameTime": 4,
        "idMatch": 199677,
        "idPlayer": 33621,
        "idTeam": 1010,
        "timePlayInMinutes": 12,
        "timePlayInSeconds": 720,
        "varDecisionExclude": false,
        "varDecisionInclude": false
    },
    {
        "cardType": "YELLOW",
        "id": 68991395,
        "idGameTime": 4,
        "idMatch": 199677,
        "idPlayer": 50804,
        "idTeam": 1012,
        "timePlayInMinutes": 31,
        "timePlayInSeconds": 1881,
        "varDecisionExclude": false,
        "varDecisionInclude": false
    },
    {
        "cardType": "RED",
        "id": 68992098,
        "idGameTime": 4,
        "idMatch": 199677,
        "idPlayer": 69281,
        "idTeam": 1012,
        "timePlayInMinutes": 44,
        "timePlayInSeconds": 2643,
        "varDecisionExclude": false,
        "varDecisionInclude": false
    },
    {
        "cardType": "YELLOW",
        "id": 68992598,
        "idGameTime": 4,
        "idMatch": 199677,
        "idPlayer": 80708,
        "idTeam": 1012,
        "timePlayInMinutes": 50,
        "timePlayInSeconds": 1875,
        "varDecisionExclude": false,
        "varDecisionInclude": false
    },
    {
        "cardType": "RED",
        "id": 68992597,
        "idGameTime": 4,
        "idMatch": 199677,
        "idPlayer": 67363,
        "idTeam": 1010,
        "timePlayInMinutes": 50,
        "timePlayInSeconds": 900,
        "varDecisionExclude": false,
        "varDecisionInclude": false
    }
]


export default getCards;